CREATE TABLE IF NOT EXISTS "users" (
  "id" serial NOT NULL PRIMARY KEY,
  "email" character(255) NOT NULL,
  "first_name" character(255) NOT NULL,
  "last_name" character(255) NOT NULL,
  "username" character(255) NOT NULL
);

INSERT INTO users (email, first_name, last_name, username) values ('dhiraj2@devglan.com', 'Dhiraj', 'Ray', 'only2dhir');
INSERT INTO users (email, first_name, last_name, username) values ('mike2@gmail.com', 'Mike', 'Huss', 'mikehuss');
