package fr.demo.veloscal.model;

import javax.naming.Name;

import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

/**
 * objectclass : L'attribut objectclass désigne la ou les classes associées au noeud
 * cn: Le common name ou nom commum ex: personne (une sorcière, le boulanger, …)
 * sn: surname est le nom de famille ex: sow ou Ben Alex
 * DN: est l'identifiant d'une entrée LDAP. C'est un chemin dans l'arborescence de l'annuaire.
 * Exemple: dn: uid=bob,ou=people,dc=springframework,dc=org
 * uid (userid), il s'agit d'un identifiant unique obligatoire
 */
@Entry(base = "ou=users", objectClasses = { "person", "inetOrgPerson", "top" })
public class LdapUser {

    @Id
    private Name id;

    @Attribute(name = "uid")
    private String userid;

    @Attribute(name = "cn")
    private String username;

     @Attribute(name = "sn")
     private String nom;

    @Attribute(name = "userPassword")
    private String password;

    public LdapUser() {
    }

    public LdapUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Name getId() {
        return id;
    }

    public void setId(Name id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "LdapUser {" +
                "id = " + id +
                ", userid = '" + userid + '\'' +
                ", username = '" + username + '\'' +
                ", nom = '" + nom + '\'' +
                ", password = '" + password + '\'' +
                '}';
    }
}
