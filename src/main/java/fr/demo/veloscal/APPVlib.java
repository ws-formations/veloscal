package fr.demo.veloscal;

import fr.demo.veloscal.conf.LdapClient;
import fr.demo.veloscal.dao.ldap.LdapUserRepository;
import fr.demo.veloscal.model.LdapUser;
import fr.demo.veloscal.rest.HomeController;
import fr.demo.veloscal.service.impl.LdapUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import java.util.List;

@PropertySource("classpath:application.properties")
@EnableAutoConfiguration
@SpringBootConfiguration
@EntityScan(value = "fr.demo.veloscal.model")
@ComponentScan(basePackages = "fr.demo.veloscal")
@EnableLdapRepositories(basePackages = "fr.demo.veloscal.dao.ldap")
@EnableJpaRepositories(basePackages = "fr.demo.veloscal.dao.jpa")
public class APPVlib{

	public static void main(String[] args) {
		SpringApplication.run(APPVlib.class, args);
	}

}
