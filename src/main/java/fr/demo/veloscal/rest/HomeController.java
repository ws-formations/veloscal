package fr.demo.veloscal.rest;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;


@RestController()
public class HomeController {

	 private static Logger log = LoggerFactory.getLogger(HomeController.class);

	 /**
		* Paramettre HttpServletRequest request
		* @return
		*/
	 @GetMapping("/")
	 public String index() {

			log.info("Getting UsernamePasswordAuthenticationToken from SecurityContextHolder");

		  // String u1 = request.getRemoteUser();
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			//Authentication auths = (Authentication) request.getUserPrincipal();

			log.info("Getting principal from UsernamePasswordAuthenticationToken");
			LdapUserDetailsImpl principal = (LdapUserDetailsImpl) auth.getPrincipal();

			log.info("authentication: {}  ", auth.toString());
			log.info("principal: {} ", principal.toString());
			log.info("principal -> DN: {} ", principal.getDn());
			log.info("principal -> USERNAME: {} ", principal.getUsername());
			log.info("principal -> PASSWORD: {} ", principal.getPassword());
			log.info("principal -> Authorities: {} ", principal.getAuthorities());

			return "Spring Security + Spring LDAP Authentication Configuration Example";
	 }


	 @GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("custom-login");
		return mav;
	}

	@GetMapping("/secure/man")
	public ModelAndView welcome1(Authentication authentication) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("info");
		 index();
		return mav;
	}

	@GetMapping("/secure/dev")
	public ModelAndView welcome2(Authentication authentication) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("info");
		 index();
		return mav;
	}

	@GetMapping("/accessDenied")
	public ModelAndView error() {
		ModelAndView mav = new ModelAndView();
		String errorMessage = "You are not authorized to access this page.";
		mav.addObject("errorMsg", errorMessage);
		mav.setViewName("access-denied");
		return mav;
	}
}
