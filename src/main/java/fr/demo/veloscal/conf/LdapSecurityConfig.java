package fr.demo.veloscal.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class LdapSecurityConfig extends WebSecurityConfigurerAdapter {

	 @Value("${ldap.urls}")
	 private String ldapUrl;

	 @Value("${ldap.user.dn.patterns}")
	 private String userDnPatterns;

	 @Value("${ldap.group.search.base}")
	 private String groupSearchBase;

	 @Value("${ldap.user.search.base}")
	 private String userSearchBase;

	 @Value("${ldap.groupe.search.filter}")
	 private String groupSearchFilter;

	 @Value("${ldap.user.search.filter}")
	 private String userSearchFilter;

	 @Value("${ldap.ldif}")
	 private String ldif;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
						.antMatchers("static/**").permitAll()
						.antMatchers("templates/**").permitAll()
		.antMatchers("/secure/man/**").access("hasRole('MANAGERS')")
		.antMatchers("/secure/dev/**").access("hasRole('DEVELOPERS')")
		.and().formLogin()
        .loginPage("/login")
        .loginProcessingUrl("/appLogin")
        .usernameParameter("username")
        .passwordParameter("password")
        .defaultSuccessUrl("/secure/dev")	
		.and().logout()
		.logoutUrl("/appLogout") 
		.logoutSuccessUrl("/login")
		.and().exceptionHandling()
		.accessDeniedPage("/accessDenied");
	} 	

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.ldapAuthentication()
	        .userDnPatterns(userDnPatterns)
	        .userSearchBase(userSearchBase)
	        .userSearchFilter(userSearchFilter)
	        .groupSearchBase(groupSearchBase)
	        .groupSearchFilter(groupSearchFilter)
	        .contextSource()
	        .url(ldapUrl)
	        .and()
	        .passwordCompare()
	        .passwordEncoder(passwordEncoder())
	        .passwordAttribute("userPassword");		
	}


	 @Bean
	public PasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder;
	}

	 @Bean
	 public BCryptPasswordEncoder bcryptEncoder() {
			return new BCryptPasswordEncoder();
	 }
} 
