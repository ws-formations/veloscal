package fr.demo.veloscal;

import fr.demo.veloscal.model.LdapUser;
import fr.demo.veloscal.service.impl.LdapUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class APPVlibCmdLine implements CommandLineRunner {

    private static Logger log = LoggerFactory.getLogger(APPVlibCmdLine.class);

    @Autowired
    private LdapUserService userRepository;

    @Override
    public void run(String... args) {
        List<LdapUser> ldapUsers = userRepository.findAll();
        ldapUsers.forEach(user -> log.info("LDAP USER = {}", user));

    }
}
