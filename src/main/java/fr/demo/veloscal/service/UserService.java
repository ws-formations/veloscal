package fr.demo.veloscal.service;


import fr.demo.veloscal.dto.UserDto;
import fr.demo.veloscal.model.User;

import java.util.List;

public interface UserService {

    List<UserDto> findAll();
    UserDto findOne(long id);
}
