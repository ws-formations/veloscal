package fr.demo.veloscal.service.impl;

import fr.demo.veloscal.dao.ldap.LdapUserRepository;
import fr.demo.veloscal.model.LdapUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LdapUserService {
    @Autowired
    private LdapUserRepository userRepository;

    public Boolean authenticate(final String username, final String password) {
        LdapUser user = userRepository.findByUsernameAndPassword(username, password);
        return user != null;
    }

    public List<LdapUser> findAll() {
        Iterable<LdapUser> userList = userRepository.findAll();
        if (userList == null) {
            return Collections.emptyList();
        }

        return (List<LdapUser>) userList;
    }

    public List<String> search(final String username) {
        List<LdapUser> userList = userRepository.findByUsernameLikeIgnoreCase(username);
        if (userList == null) {
            return Collections.emptyList();
        }

        return userList.stream()
                .map(LdapUser::getUsername)
                .collect(Collectors.toList());
    }

    public void create(final String username, final String password) {
        LdapUser newUser = new LdapUser(username,digestSHA(password));
        newUser.setId(LdapUtils.emptyLdapName());
        userRepository.save(newUser);

    }

    public void modify(final String username, final String password) {
        LdapUser user = userRepository.findByUsername(username);
        user.setPassword(password);
        userRepository.save(user);
    }

    private String digestSHA(final String password) {
        String base64;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA");
            digest.update(password.getBytes());
            base64 = Base64.getEncoder()
                    .encodeToString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return "{SHA}" + base64;
    }
}
