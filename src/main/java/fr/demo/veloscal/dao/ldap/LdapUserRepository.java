package fr.demo.veloscal.dao.ldap;

import fr.demo.veloscal.model.LdapUser;
import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface LdapUserRepository extends LdapRepository<LdapUser>{

    LdapUser findByUsername(String username);

    LdapUser findByUsernameAndPassword(String username, String password);

    List<LdapUser> findByUsernameLikeIgnoreCase(String username);
}
