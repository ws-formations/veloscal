FROM amazoncorretto:11.0.8
RUN echo "Image openJdk amazon amazoncorretto"
VOLUME /tmp
MAINTAINER Mamadou Oury SOW <mamadououry.sow@scalian.com>

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /usr/local/share/app.jar
ENTRYPOINT ["java","-jar","/usr/local/share/app.jar"]
EXPOSE 8081


##  docker build -t ourysow/veloscal .
##  docker run --name ct-veloscal -p 9080:8080 ourysow/veloscal
##  docker exec -it veloscal /bin/bash

## docker rmi -f $(docker images -a -q)
## java -jar veloscal-0.0.1-SNAPSHOT.jar
## java -jar app.jar
##  docker login -u YOUR-USER-NAME

##  docker tag docker-sboot ourysow/docker-sboot
##  docker push ourysow/docker-sboot:tagname

##  docker run --name docker-sboot -p 8080:80 ourysow/docker-sboot
## mourysow  Lamarana@20

# docker build -t getting-started .
# docker run -dp 3000:3000 getting-started
# docker stop <the-container-id>
# docker rm <the-container-id>
